const childProcess = require('child_process');
const fs = require('fs');
const os = require('os');
const { resolve, join } = require('path');

const argv = process.argv.slice(2);
const basePath = resolve(__dirname, './services/');
const command = os.platform().startsWith('win') ? 'npm.cmd' : 'npm';

fs.readdirSync(basePath).forEach(function(service) {
    let servicePath = join(basePath, service);

    if (!fs.existsSync(join(servicePath, 'package.json'))) {
        return;
    }

    childProcess.spawn(command, argv, {
        env: process.env,
        cwd: servicePath,
        stdio: 'inherit'
    });
})
