# Desafio Intera

Este projeto implementa o [desafio proposto](https://drive.google.com/file/d/16NgqMzE8EMJpfWZEoRy4is9vYM06mY0f/view) para o processo seletivo da [Intera](http://byintera.in/) para vaga de _Tech Lead_.

O projeto está dividido em microserviços serverless dispostos na pasta [services/] deste projeto. Cada serviço foi projetado para possuir seu próprio repositório, porém foram agregados aqui para simplificação da avaliação.

# Requisitos

Este projeto requer a instalação do NodeJS + NPM e do Docker na máquina para funcionar.
Os arquivos necessários para instalação do NodeJS podem ser encontrados [aqui](https://nodejs.org/en/download/).
Para instalar o Docker siga os passos descritos [aqui](https://docs.docker.com/get-docker/).

# Ambiente de Desenvolvimento

## Copiar o projeto

Execute o comando abaixo para copiar o projeto para a máquina local:

```
$ git clone git@gitlab.com:coisa/desafio-intera.git
```

## Instalação do Projeto

Após instalado os NodeJS e o NPM basta rodar o comando descrito abaixo:

```
$ npm install
```

## Rodar em modo Offline

Para iniciar o ambiente de desenvolvimento em modo offline execute o comando descrito abaixo:

```
$ npm start
```

## Deploy Manual

Para realizar _deploy_ manualmente você precisa primeiramente configurar suas credenciais da AWS, seguindo os passos descritos [neste link](https://www.serverless.com/framework/docs/providers/aws/guide/credentials/).

Após a configuração de suas credenciaais basta executar o comando descrito abaixo:

```
$ npm run deploy
```

Caso queira realizar _deploy_ de um _stage_ específico utilize o comando abaixo:

```
$ npm run deploy -- --stage=nome_do_stage
```

### Removendo Deploy

Para remover uma _stack_ que foi realizado _deploy_ utilize o comando descrito abaixo:

```
$ npm run deploy:remove
```

Ou caso queira remover uma _stack_ de um _stage_ específico:

```
$ npm run deploy:remove -- --stage=nome_do_stage
```
