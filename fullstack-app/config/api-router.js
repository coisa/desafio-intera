module.exports = ({ talentsController, openingsController }) => (router) => {
  // Talents API
  router.get('/talents', talentsController.listAll)
  router.get('/talents/:id', talentsController.getByRequestParamId)
  router.post('/talents', talentsController.createWithRequestBody)
  router.put('/talents', talentsController.updateByRequestParamIdWithRequestBody)
  router.delete('/talents/:id', talentsController.deleteByRequestParamId)

  // Openings API
  router.get('/openings', openingsController.listAll)
  router.get('/openings/:id', openingsController.getByRequestParamId)
  router.post('/openings', openingsController.createWithRequestBody)
  router.put('/openings', openingsController.updateByRequestParamIdWithRequestBody)
  router.delete('/openings/:id', openingsController.deleteByRequestParamId)
}
