const { Router } = require('express')

module.exports = ({ apiMiddleware, apiRouter }) => {
  const router = new Router()

  apiMiddleware(router)
  apiRouter(router)

  return router
}
