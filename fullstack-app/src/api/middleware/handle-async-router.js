module.exports = ({ router, errorHandler }) => (request, response, next) => {
  try {
    return Promise
      .resolve(router(request, response, next))
      .catch((error) => errorHandler(error, request, response))
  } catch (error) {
    return errorHandler(error, request, response)
  }
}
