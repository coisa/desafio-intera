const httpStatus = require('http-status')

module.exports = () => (thrownError, request, response, next) => {
  const {
    name,
    statusCode = httpStatus.INTERNAL_SERVER_ERROR,
    message = httpStatus[`${httpStatus.INTERNAL_SERVER_ERROR}_MESSAGE`]
  } = thrownError

  const responseBody = {
    name: name || httpStatus[`${statusCode}_NAME`],
    statusCode,
    message
  }

  if (process.env.IS_OFFLINE) {
    responseBody.stack = String(thrownError.stack)
  }

  console.log('error-handler', thrownError);

  response.status(statusCode).json({ errors: [responseBody] })
}
