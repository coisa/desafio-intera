module.exports = ({ abstractController, openingService }) => {
  const controller = new abstractController(openingService)

  return {
    listAll: (...args) => controller.listAll(...args),
    getByRequestParamId: (...args) => controller.getByRequestParamId(...args),
    createWithRequestBody: (...args) => controller.createWithRequestBody(...args),
    updateByRequestParamIdWithRequestBody: (...args) => controller.updateByRequestParamIdWithRequestBody(...args),
    deleteByRequestParamId: (...args) => controller.deleteByRequestParamId(...args)
  }
}
