const dynamoose = require('dynamoose')

module.exports = ({ dynamodb }) => {
  dynamoose.aws.ddb.set(dynamodb)

  if (process.env.IS_OFFLINE) {
    dynamoose.aws.ddb.local()
  }

  return dynamoose
}
