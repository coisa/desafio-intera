const { DynamoDB } = require('aws-sdk')

module.exports = () => {
  const options = {}

  if (process.env.IS_OFFLINE) {
    options.endpoint = `http://0.0.0.0:${process.env.DYNAMODB_PORT}`
  }

  return new DynamoDB(options)
}
