module.exports = ({ dynamoose }) => new dynamoose.Schema({
  title: {
    type: String,
    required: true
  },
  minimunExperienceYears: {
    type: Number,
    required: true
  }
})
