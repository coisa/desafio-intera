module.exports = ({ dynamoose }) => new dynamoose.Schema({
  title: {
    type: String,
    required: true
  },
  since: {
    type: Date,
    required: true
  }
})
