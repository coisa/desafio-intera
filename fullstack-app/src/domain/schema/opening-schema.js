module.exports = ({ dynamoose, requirementSchema }) => new dynamoose.Schema({
  id: {
    type: String,
    hashKey: true
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  requirements: {
    type: Array,
    schema: [requirementSchema],
    required: true
  }
}, {
  saveUnknown: true,
  timestamps: true
})
