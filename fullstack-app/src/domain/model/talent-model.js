module.exports = ({ createModel, talentSchema }) => createModel(process.env.TALENTS_TABLE_NAME, talentSchema)
