module.exports = ({ createModel, openingSchema }) => createModel(process.env.OPENINGS_TABLE_NAME, openingSchema)
