const { isISO8601, toDate, trim } = require('validator')

module.exports = () => (data) => {
  const { title, since } = data

  if (typeof title !== 'string' && !(title instanceof String)) {
    throw new Error('You should give an attribute "title" with a string value')
  }

  if (!since || !isISO8601(since, 'YYYY-MM-DD')) {
    throw new Error('You should give an attribute "since" with a string ISO8601 date formatted (aka.: Y-m-d)')
  }

  return {
    title: trim(title),
    since: toDate(since)
  }
}
