module.exports = () => (event, context, callback) => {
  const response = {
    statusCode: 200,
    body: JSON.stringify({
      message: 'SQS event processed.',
      input: event,
    }),
  };

  console.log('event: ', JSON.stringify(event));

  event.Records.forEach(record => {
    // @TODO verifica se é registro de talento para buscar vagas que preencham os requisitos
    // @TODO verifica se é registro de vaga para buscar talentos que possuam as habilidades
    console.log("message received: ", record);
  });

  callback(null, response);
};
