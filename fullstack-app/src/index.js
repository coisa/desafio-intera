const container = require('./container')

module.exports = {
  handler: container.resolve('handler'),
  worker: container.resolve('matchingQueueWorker')
}
