describe('Lambda Handler', () => {
  it('should throw Error platform not supported', () => {
    const platform = Math.random().toString(36)
    process.env.SERVERLESS_EXPRESS_PLATFORM = platform

    expect.assertions(1)

    try {
      require('../index')
    } catch (e) {
      expect(e.message).toBe(`${platform} is not handled properly by serverless-express`)
    }
  })
})
