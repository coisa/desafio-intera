## Instalação do Projeto

Rodar o comando descrito abaixo:

```
$ make install
```

## Rodar em modo Offline

Para iniciar o ambiente de desenvolvimento em modo offline execute o comando descrito abaixo:

```
$ npm start
```

## Deploy Manual

Para realizar _deploy_ manualmente você precisa primeiramente configurar suas credenciais da AWS, seguindo os passos descritos [neste link](https://www.serverless.com/framework/docs/providers/aws/guide/credentials/).

Após a configuração de suas credenciaais basta executar o comando descrito abaixo:

```
$ npm run deploy
```

Caso queira realizar _deploy_ de um _stage_ específico utilize o comando abaixo:

```
$ npm run deploy -- --stage=nome_do_stage
```

### Removendo Deploy

Para remover uma _stack_ que foi realizado _deploy_ utilize o comando descrito abaixo:

```
$ npm run deploy:remove
```

Ou caso queira remover uma _stack_ de um _stage_ específico:

```
$ npm run deploy:remove -- --stage=nome_do_stage
```
