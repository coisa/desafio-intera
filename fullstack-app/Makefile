.DEFAULT_GOAL := start

DOCKER_BIN := $(shell which docker)
DOCKER_COMPOSE_BIN := $(shell which docker-compose)

ifeq ($(strip $(DOCKER_BIN)),)
	DOCKER_EXISTS := @echo "\nERROR:\n docker not found.\n See: https://docs.docker.com/get-docker/\n" && exit 1
endif
ifeq ($(strip $(DOCKER_COMPOSE_BIN)),)
	DOCKER_COMPOSE_EXISTS := @echo "\nERROR:\n docker-compose not found.\n See: https://docs.docker.com/compose/install/\n" && exit 1
endif

export UID=$(shell id -u)
export GID=$(shell id -g)

.PHONY: precheck
precheck: 
	$(DOCKER_EXISTS)
	$(DOCKER_COMPOSE_EXISTS)

.PHONY: install
install: precheck build node_modules

.PHONY: uninstall
.SILENT: uninstall
uninstall:
	-$(DOCKER_COMPOSE_BIN) down
	-rm -r node_modules
	-rm .env

.PHONY: reinstall
reinstall: uninstall install

.PHONY: build
build: .env
	$(DOCKER_COMPOSE_BIN) build

.PHONY: start
start: install
	$(DOCKER_COMPOSE_BIN) up -d

node_modules: precheck
	$(DOCKER_BIN) run  --interactive --tty --rm --volume "$(PWD):/usr/src/app:Z" --workdir "/usr/src/app" --user $(UID):$(GID) node:14-alpine npm install

.env:
	cp .env.dist .env