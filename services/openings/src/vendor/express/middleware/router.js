const { Router } = require('express')

module.exports = ({ middlewares, routes }) => {
  const router = new Router()

  middlewares(router)
  routes(router)

  return router
}
