const express = require('serverless-express/express')
const httpStatus = require('http-status')

module.exports = ({ handleAsyncRouter, errorHandler }) => {
  const app = express()

  app.use(function (request, response, next) {
    response.header('x-powered-by', 'serverless-express')
    next()
  })

  // Async Handler
  app.use(handleAsyncRouter)

  // Fallback RequestHandler
  app.all('*', (request, response, next) => response.sendStatus(httpStatus.NOT_FOUND))

  // Error Handler
  app.use(errorHandler)

  return app
}
