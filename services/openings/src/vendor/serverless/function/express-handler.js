const handler = require('serverless-express/handler')

module.exports = ({ expressApp }) => handler(expressApp)
