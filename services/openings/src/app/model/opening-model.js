module.exports = ({ dynamoose, openingSchema }) => dynamoose.model(process.env.TABLE_NAME, openingSchema)
