module.exports = ({ abstractService, openingModel, openingValidator }) => {
  const [ model, validator ] = [ openingModel, openingValidator ]

  const queueUrl = process.env.QUEUE_URL
  const service = new abstractService(model, validator, queueUrl)

  return {
    create: (...args) => service.create(...args),
    getAll: (...args) => service.getAll(...args),
    getById: (...args) => service.getById(...args),
    deleteById: (...args) => service.deleteById(...args)
  }
}
