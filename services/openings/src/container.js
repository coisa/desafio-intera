const path = require('path')
const awilix = require('awilix')

const container = awilix.createContainer()

container.loadModules([
  path.join(__dirname, '/../config/*.js'),
  path.join(__dirname, '**', '*.js')
], {
  formatName: 'camelCase',
  lifetime: awilix.Lifetime.SINGLETON,
})

module.exports = container
