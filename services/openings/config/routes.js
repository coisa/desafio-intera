module.exports = ({ openingsController }) => (router) => {
  router.get('/openings', openingsController.listAll)
  router.get('/openings/:id', openingsController.getByRequestParamId)
  router.post('/openings', openingsController.createWithRequestBody)
  router.put('/openings', openingsController.updateByRequestParamIdWithRequestBody)
  router.delete('/openings/:id', openingsController.deleteByRequestParamId)
}
