module.exports = () => (middleware) => {
  middleware.use(require('body-parser').json())
  middleware.use(require('method-override')())
  middleware.use(require('response-time')())
  middleware.use(require('cors')())
  middleware.use(require('morgan')('combined'))
  middleware.use(require('apicache').middleware('5 minutes', (req, res) => res.statusCode === 200))
}
