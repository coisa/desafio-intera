const handler = require('../index').handler;

describe('Lambda Handler', () => {
    it('should console.log event Records', () => {
      const consoleSpy = jest.spyOn(console, 'log');

      const event = {
        Records: [Math.random().toString(36), Math.random().toString(36), Math.random().toString(36)]
      };

      handler(event, {})

      event.Records.forEach((record) => {
        expect(consoleSpy).toHaveBeenCalledWith(record);
      });
    });
});
