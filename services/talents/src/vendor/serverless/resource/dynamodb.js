const { DynamoDB } = require('aws-sdk')

module.exports = () => {
  const options = {
    region: process.env.REGION
  }

  if (process.env.IS_OFFLINE) {
    options.endpoint = 'http://0.0.0.0:8000'
  }

  return new DynamoDB(options)
}
