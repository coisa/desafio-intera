const { SQS } = require('aws-sdk')

module.exports = () => {
  let options = {
    region: process.env.REGION
  }

  if (process.env.IS_OFFLINE) {
    options.endpoint = 'http://localhost:9324'
  }

  return new SQS(options)
}
