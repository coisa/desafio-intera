module.exports = ({ sqs }) => (queueUrl) => {
  const sendMessage = async (messageBodyObject) => {
    const params = Object.assign({}, {
      MessageBody: JSON.stringify(messageBodyObject),
      QueueUrl: queueUrl
    })

    await sqs.sendMessage(params)
  }

  return { url: queueUrl, sendMessage }
}
