const { isEmail, normalizeEmail, trim } = require('validator')
const httpStatus = require('http-status')

module.exports = ({ skillValidator }) => (data) => {
  let { name, email, skills } = data

  try {
    if (typeof name !== 'string' && !(name instanceof String)) {
      throw new Error('You should give an attribute "name" with a string value')
    }

    if (!email || !isEmail(email)) {
      throw new Error('You should give a valid email attribute "email"')
    }

    if (!skills || !Array.isArray(skills)) {
      throw new Error('You should give an array of "skills" attribute')
    }

    skills = skills.map((skill, index) => {
      try {
        return skillValidator(skill)
      } catch (err) {
        throw new Error(`The given "skill[${index}]" has an invalid format: ${err.message}`)
      }
    })
  } catch (err) {
    err.statusCode = httpStatus.BAD_REQUEST
    throw error
  }

  return {
    name: trim(name),
    email: normalizeEmail(email),
    skills,
  }
}
