const uuid = require('uuid')

module.exports = ({ queue }) => class AbstractService {
  constructor (model, validator, queueUrl) {
    this.model = model
    this.validator = validator
    this.queue = queue(queueUrl)
  }

  async create (attributes) {
    console.log(this.validator, this.model, this.queue)

    const data = Object.assign({ id: uuid.v4() }, this.validator(attributes))
    const entity = await this.model.create(data)

    try {
      await this.queue.sendMessage(entity)

      console.log(`Talent ${entity.id} sent to queue ${this.queue.url}`)
    } catch (error) {
      console.error(`Error trying to send Talent ${entity.id} to queue ${this.queue.name}. Message: ${error.message}`)
    }

    return entity
  }

  async getAll () {
    return await this.model.scan().exec()
  }

  async getById (id) {
    const entity = await this.model.query(id).exec()
    entity.populate()

    return entity
  }

  async deleteById (id) {
    await this.model.delete(id)
  }
}
