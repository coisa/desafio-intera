module.exports = ({ dynamoose, skillSchema }) => new dynamoose.Schema({
  id: {
    type: String,
    hashKey: true
  },
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  skills: {
    type: Array,
    schema: [skillSchema],
    required: true
  }
}, {
  saveUnknown: true,
  timestamps: true
})
