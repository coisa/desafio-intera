const httpStatus = require('http-status')

module.exports = () => class AbstractController {
  constructor (service) {
    this.service = service
  }

  async listAll (request, response, next) {
    try {
      // @TODO filtering, sorting, and pagination
      const collection = await this.service.getAll()

      return response
        .status(httpStatus.OK)
        .json(collection)
    } catch (error) {
      return next(error, null)
    }
  }

  async getByRequestParamId (request, response, next) {
    try {
      const id = request.param.id
      const item = await this.service.getById(id)

      console.log(item)

      return response.json(item.toJSON())
    } catch (error) {
      return response.sendStatus(httpStatus.NOT_FOUND)
    }
  }

  async createWithRequestBody (request, response, next) {
    try {
      const attributes = request.body

      if ('id' in attributes) {
        const error = new Error(httpStatus[`${code}_MESSAGE`])

        error.code = httpStatus.BAD_REQUEST
        error.name = httpStatus[`${httpStatus.BAD_REQUEST}_NAME`]

        throw error
      }

      const item = await this.service.create(attributes)

      // @TODO add Location Header
      return response
        .status(httpStatus.CREATED)
        .json(item.toJSON())
    } catch (error) {
      return next(error, null)
    }
  }

  async updateByRequestParamIdWithRequestBody (request, response, next) {
    try {
      const id = request.params.id
      const item = await this.service.update(id, request.body)

      return response
        .status(httpStatus.OK)
        .json(item.toJSON())
    } catch (error) {
      return next(error, null)
    }
  }

  async deleteByRequestParamId (request, response, next) {
    try {
      const id = request.params.id

      this.service.getById(id)
    } catch (error) {
      console.warn(error)

      return response.sendStatus(httpStatus.NO_CONTENT)
    }

    try {
      await this.service.deleteById(id)

      return response.sendStatus(httpStatus.OK)
    } catch (error) {
      return next(error, null)
    }
  }
}
