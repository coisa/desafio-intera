module.exports = ({ abstractController, talentService }) => {
  const controller = new abstractController(talentService)

  return {
    listAll: (...args) => controller.listAll(...args),
    getByRequestParamId: (...args) => controller.getByRequestParamId(...args),
    createWithRequestBody: (...args) => controller.createWithRequestBody(...args),
    updateByRequestParamIdWithRequestBody: (...args) => controller.updateByRequestParamIdWithRequestBody(...args),
    deleteByRequestParamId: (...args) => controller.deleteByRequestParamId(...args)
  }
}
