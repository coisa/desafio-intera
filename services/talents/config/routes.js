module.exports = ({ talentsController }) => (router) => {
  router.get('/talents', talentsController.listAll)
  router.get('/talents/:id', talentsController.getByRequestParamId)
  router.post('/talents', talentsController.createWithRequestBody)
  router.put('/talents', talentsController.updateByRequestParamIdWithRequestBody)
  router.delete('/talents/:id', talentsController.deleteByRequestParamId)
}
